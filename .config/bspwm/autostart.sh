#!/bin/bash

sxhkd &
picom &
pipewire &
pipewire-pulse &
lxpolkit &
dunst &
fcitx5 &
playerctld daemon &

$HOME/.local/bin/locker.sh &

feh --bg-fill $HOME/pictures/wallpapers/5.jpg &

# Statusbar
EWW="$HOME/.local/bin/eww"
EWW_CONFIG="$HOME/.config/eww/"

killall -q ${EWW}
${EWW} -c ${EWW_CONFIG} daemon
${EWW} -c ${EWW_CONFIG} open bar

xsetroot -cursor_name left_ptr &
#xautolock -time 10 -locker "betterlockscreen -l dim" -killtime 20 -killer "loginctl suspend" -detectsleep &
#xidlehook --detect-sleep --not-when-fullscreen --not-when-audio --timer 30 'xset dpms force off' '' --timer 30 'betterlockscreen -l dim' '' --timer 60 'loginctl suspend' '' &
