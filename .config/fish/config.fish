set fish_greeting

set -e fish_user_paths
set -U fish_user_paths PATH $HOME/.local/bin $fish_user_paths
set -U fish_user_paths PATH $HOME/.nix-profile/bin $fish_user_paths
set -U fish_user_paths PATH $HOME/.cargo/bin $fish_user_paths
set -x XDG_CURRENT_DESKTOP bspwm

set -gx EDITOR lvim

# ALIAS #
alias reboot='loginctl reboot'
alias poweroff='loginctl poweroff'
alias suspend='loginctl suspend'

alias xi='sudo xbps-install -Rs'
alias xq='sudo xbps-query'
alias xrs='sudo xbps-query -Rs'
alias xr='sudo xbps-remove -R'
alias xu='sudo xbps-install -Suv'

alias bt='rofi-bluetooth'
alias wf='rofi-wifi-menu.sh'

alias vim='lvim'
alias code='code-oss'
alias vpn='sudo openvpn --config $HOME/Downloads/us-free-04.protonvpn.net.udp.ovpn --auth-user-pass $HOME/Documents/pass.txt'

alias fetch='neofetch --kitty --source pictures/chae.jpg --size 16%'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

starship init fish | source

fish_add_path /home/kara/.spicetify
